


import Todos from './todos.api';

const fs = require('fs');
// const pdfParser = require('pdf-parse');


const Companies = new Mongo.Collection('companies');


//APIs for the companies collection, defining schemas and available methods for our app to be used.

Companies.schema = new SimpleSchema({
    name: {type: String},
    industry: {type: String},
    date_created: {type: String},
    pdf: {type: String, optional: true},
    representatives: {type: [Object], optional: true},
    "representatives.$.name": {
        type: String,
        optional: true
    },
    "representatives.$.id": {
        type: String,
        optional: true
    },
    "representatives.$.nationality": {
        type: String,
        optional: true
    },
    "representatives.$.position": {
        type: String,
        optional: true
    },
    "representatives.$.date_of_appointment": {
        type: String,
        optional: true
    },
});

if (Meteor.isServer) {
    // This code only runs on the server
  
    const PDFParser = require("pdf2json");

   Meteor.methods({
    'companies.insert'(data, pdf) {
        let pdfParser = new PDFParser();
        const date_created = new Date();
        data.date_created = `${date_created}`;
        
        return new Promise( (resolve, reject) => {
            if(pdf) {
                data.pdf = `${process.cwd()}/${pdf.file}`;
               
                //Bind the fs async methods with Meteor
                fs.writeFile = Meteor.wrapAsync(fs.writeFile.bind(fs));
                //Write the uploaded pdf, this can be written or stored in a cloud storage eg. AWS S3

                try{
                    fs.writeFile(pdf.file, pdf.result, 'binary', (err)=>{
                            if( err) throw new Meteor.Error(' there was a problem saving the pdf');
                            //Bind the fs async methods with Meteor
                            pdfParser.on = Meteor.wrapAsync( pdfParser.on.bind(pdfParser));

                            try{

                                pdfParser.on("pdfParser_dataError", err => console.error(err.parserError) );
                                pdfParser.on("pdfParser_dataReady", pdfData => {
                                        let regExp = new RegExp('Representative(s)', 'i');
                                        let _objects = [];
                                        let object = {};
                    
                                        pdfData.formImage.Pages.forEach( (page, pageIndex, pages) => {
                                            let begin = null;
                                            let ending = null;
                                            if(page.Texts) {
                                                page.Texts.forEach( (text, textIndex) => {
                                                    // console.log('# text', text.R[0].T);
                                                    let textNode = text.R[0].T;
                                                    
                                                    if(textNode === 'Officers%2FAuthorised%20Representative(s)') {
                                                        // console.log('# textNode ', textNode);
                                                        // console.log('# textID', page.Texts[textIndex - 1].R[0].T)
                    
                                                        let t_id = page.Texts[textIndex - 1];
                    
                                                        begin = textIndex - 1;
                    
                                                    } 
                    
                                                    if( textNode === 'Shareholder(s)') {
                                                        ending = textIndex;
                                                    }
                                                
                                                })
                                            }
                    
                                            if(begin) {
                                                if(!ending ) {
                                                    ending = (page.Texts.length - 1) - 1;
                                                }
                                                // console.log('######## begin to slice', begin)
                                                // console.log('######## ending to slice', ending)
                                                
                                                let _slicedTexts = page.Texts.slice(begin, ending);
                    
                                                let _idBaseX;
                                                let _nameBaseX;
                                                let _addressBaseX;
                                                let _nationalityBaseX;
                                                let _positionHeldBaseX;
                                                let _dateOfAppointmentBaseX;
                    
                                                let _idList = [];
                                                let _nameList = [];
                                                let _addressList = [];
                                                let _nationalityList = [];
                                                let _positionHeldList = [];
                                                let _dateOfAppointmentList = [];
                    
                                                _slicedTexts.forEach( text => {
                                                    let textNode = text.R[0].T;
                                                    
                                                    if(textNode === 'ID') {
                                                        _idBaseX = text.x;
                                                    }
                    
                                                    if( textNode === 'Name') {
                                                        _nameBaseX = text.x;
                                                    }
                    
                                                    if( textNode === 'Address') {
                                                        _addressBaseX = text.x;
                                                    }
                    
                                                    if( textNode === 'Nationality') {
                                                        _nationalityBaseX = text.x;
                                                    }
                    
                                                    if( textNode === 'Position%20Held') {
                                                        _positionHeldBaseX = text.x;
                                                    }
                    
                                                    if( textNode === 'Date%20of%20Appointment') {
                                                        _dateOfAppointmentBaseX = text.x;
                                                    }
                                                    
                                                    //For ID
                                                    if(text.x === _idBaseX + .375 ) {
                                                        _idList.push(textNode);
                                                    }
                                                    //For Name
                                                    if( text.x === _nameBaseX + .375) {
                                                        _nameList.push(textNode);
                                                    }
                    
                                                    //For Address
                                                    if( text.x === _addressBaseX + .375) {
                                                        _addressList.push(textNode);
                                                    }
                                                    
                                                    //For Nationality
                                                    if( text.x === _nationalityBaseX + .375) {
                                                        _nationalityList.push(textNode);
                                                    }
                    
                                                    //For Position Held
                                                    if( text.x === _positionHeldBaseX + .375) {
                                                        _positionHeldList.push(textNode);
                                                    }
                                                    
                                                    //For Date of Appointment
                                                    if( text.x === _dateOfAppointmentBaseX + .375) {
                                                        _dateOfAppointmentList.push(textNode);
                                                    }
                    
                                
                                                })
                    
                                                _nameList = _nameList.filter( (name, id) => {
                                                    if( id === 0 || id % 5 === 0 ) {
                                                        return name;
                                                    }
                                                });
                    
                                                _addressList =  _addressList.reduce( (filtered, option, index) => {
                                                    if( index === 1 || index % 5 > 0) {
                                                        filtered.push( option );
                                                    }
                    
                                                    return filtered;
                                                }, []);
                    
                                                _nationalityList = _nationalityList.reduce( (filtered, option, index) => {
                                                    if( index === 0 || index % 2 === 0) {
                                                        filtered.push( option );
                                                    }
                    
                                                    return filtered;
                                                }, []);
                    
                                                _positionHeldList = _positionHeldList.length > 1 && ( _positionHeldList.reduce( (filtered, option, index) => {
                                                    if( index === 1 || index % 2 > 0) {
                                                        filtered.push( option );
                                                    }
                    
                                                    return filtered;
                                                }, []));
                    
                                                // console.log('#  _idList', _idList);
                                                // console.log('#  _nameList', _nameList);
                                                // console.log('#  _addressList', _addressList);
                                                // console.log('#  _nationalityList', _nationalityList);
                                                // console.log('#  _positionHeldList', _positionHeldList);
                                                // console.log('#  _dateOfAppointmentList', _dateOfAppointmentList);
                                            
                                                _objects = [
                                                    ..._objects,
                                                    ..._nameList.map( (name, ind) => {
                                                        return {
                                                            name: name,
                                                            id: _idList[ind],
                                                            nationality: _nationalityList[ind],
                                                            position: _positionHeldList[ind],
                                                            date_of_appointment:  _dateOfAppointmentList[ind],
                                                        }
                                                    })
                                                ]
                                            }
                    
                                        })

                                        data.representatives = _objects;

                                        Companies.schema.validate(data);
                    
                                        let companyInsert = Companies.insert(data); 
                                        resolve( companyInsert )
                                    
                                        // Companies.insert(data);
                                            
                                    }
                                );

                            } catch(err) {
                                console.log('# err', err)
                            }
                            

                        pdfParser.loadPDF(data.pdf);


                    });
                } catch(err) {
                    console.log('# err', err);
                }

    
            } else {
                Companies.schema.validate(data);
    
                let companyInsert = Companies.insert(data);
    
                resolve( companyInsert )

            }

        })

        

        // return companyInsert;
    },
    'companies.getById'({id: companyId, todoOptions}) {
            let pdfParser = new PDFParser();
            
            let _company = Companies.findOne(companyId);
        
            let _todos = Todos.find({
                companyId: companyId,
            }, {
                skip: todoOptions.p_size * (todoOptions.p_number - 1),
                limit: todoOptions.p_size,
            }).fetch()

            return new Promise( (resolve, reject) => {
                
                resolve( {
                    ..._company,
                    todos: [
                        ..._todos,
                    ]
                })
            })

            // return {
            //     ..._company,
            //     todos: [
            //         ..._todos,
            //     ]
            // }
        
    },

    'companies.uploadPDF': (fileInfo, fileData) => {
        fs.writeFile( fileInfo.name, 'binary', fileData);
    }
   })
}

Meteor.methods({
    
    'companies.fetch'() {
        return Companies.find().fetch();
    },
    
})

export default Companies