const Todos = new Mongo.Collection('todos');

//APIs for the todos collection, defining schemas and available methods for our app to be used.

//For each todo item, it has a many-to-one relationship with Companies collection.

Todos.schema = new SimpleSchema({
    label: {type: String},
    description: {type: String, optional: true},
    completed: {type: Boolean},
    date_created: {type: String},
    priority: {type: String},
    companyId: {type: String, regEx: SimpleSchema.RegEx.Id}
});

Meteor.methods({
    'todos.fetch'() {
        return Todos.find().fetch();
    },
    'todos.getById'({id: todoId}) {
        let _todo = Todos.findOne(todoId);
    
        return {
            ..._todo,
        }
    },
    'todos.getTodosByCompanyId'({companyId, p_size, p_number}) {
        let skip = p_size * (p_number - 1);
        let _todos = Todos.find({
            companyId: companyId,
        },{skip, limit: p_size}).fetch()
    
        return _todos;
    },
    'todos.insert'(data) { 

        const date_created = new Date();
        data.date_created = `${date_created}`;
        data.completed = false;

        Todos.schema.validate(data);

        return Todos.insert(data); 

    },
    'todos.update':  ({id, ...rest}) => {
       
        Todos.update({_id: id}, {
            $set: { ...rest },
        });
      
        return Todos.findOne(id);

    }
})

export default Todos;