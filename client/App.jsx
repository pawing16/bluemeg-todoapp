import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Route, Redirect, withRouter, Link} from 'react-router-dom';

import { List, ListItem, ListItemText, Typography, ListSubheader, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import {todos_actions, companies_actions} from './actions';

import {IndexPage, CompanyPage } from './components/pages';

const styles = theme => ({
    main: {
        width: '100vw',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 300,
    },
    navList: {
        width: 300,
        position: 'fixed',
        top: 0,
        left: 0,
        bottom: 0,
        backgroundColor: '#fff',
       paddingTop: '1em',

    }
})

class App extends Component {

    addTodo =  (payload) => {
        this.props.dispatch( todos_actions.addTodo(payload) );
    }

    getTodos =  () => {
        this.props.dispatch( todos_actions.getTodos() );
    }
    
    addCompany = (payload) => {
        this.props.dispatch( companies_actions.addCompany(payload) );
    }

    getCompany = (id) => {
        this.props.dispatch( companies_actions.getCompany(id) );
    }

    getCompanyById = (id) => {
        this.props.dispatch( companies_actions.getCompanyById(id) );
    }

    getCompanies = () => {
        this.props.dispatch( companies_actions.getCompanies() );
    }

    redirectToIndex = () => {
        const {history} = this.props;
        history.push(`/`);
    }

    navListClickHandler(id) {
        const {history} = this.props;
        history.push(`/companies/${id}`)
    }


    _renderNavList() {
        const {companies, classes, history} = this.props;
        if(!companies) {
            return null;
        }

        return (
            <List 
                component="nav"
                className={classes.navList}
                subheader={
                    <ListSubheader style={{
                        lineHeight: 1.3,
                        paddingBottom: '1em',
                    }}> 
                        { companies.list && companies.list.length > 0 ?  `Your Companies` : `You still dont have companies registered, start adding up your companies using the form.` } 
                        { companies.list && companies.list.length > 0 && history.location.pathname !== '/' && (<Button style={{
                            minWidth: '24px',
                            fontSize: '0.6125rem',
                            marginLeft: '0.2em',
                            minHeight: '24px',
                            position: 'absolute',
                            right: '5px',
                            top: '-5px',
                        }} onClick={this.redirectToIndex} size="small" color="primary">Add a new company</Button> )}
                    </ListSubheader>
                }
            >
                
                {
                    companies.list && companies.list.length > 0 && companies.list.map( company => {
                        return (
                            <ListItem 
                                key={`item_${company._id}`} 
                                button
                                onClick={
                                    () => {
                                        this.navListClickHandler(company._id)
                                    }
                                }
                            >
                                
                                    <ListItemText 
                                        key={`itemtext_${company._id}`}
                                        primary={company.name} 
                                    />
                                
                            </ListItem>
                        )
                    })
                }
               
            </List>
        )
    }

    componentWillMount() {
        //whenever the app will mount get the available companies
        this.props.dispatch( companies_actions.getCompanies() );
        // this.props.dispatch( todos_actions.getTodos() );
    }

    render() {
        const {classes, companies} = this.props;
        return(
            <Fragment>

                {
                    this._renderNavList()
                }
                
                <main 
                    className={classes.main}
                >
                    <Route exact path='/' component={IndexPage} />
                    <Route path='/companies/:companyId' component={CompanyPage} />
                </main>
                
            </Fragment>
        )
    }
}

const mapStateToProps = ( state ) => ({
    todos: state.todos,
    companies: state.companies,
})

export default withStyles(styles)(withRouter(connect(mapStateToProps)(App)));