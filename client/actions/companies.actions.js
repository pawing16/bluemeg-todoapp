import { companies_constants } from '../constants/';
import { companies_services } from '../services/';

import Utils from '../utils';

//Action for adding a company
function addCompany(payload) {

    return dispatch => {
        companies_services.addCompany(payload)
            .then(res => {
                dispatch({
                    type: companies_constants.ADD_COMPANY,
                    payload: {
                        _id: res,
                        ...payload,
                    }
                })

            }).catch(err => {
                dispatch({
                    type: companies_constants.ADD_COMPANY_FAILURE,
                    payload: null,
                })
            })

    }
}

//Action for getting all companies registered
function getCompanies() {
    return dispatch => {
        companies_services.getCompanies().then(res => {
            dispatch({
                type: companies_constants.GET_COMPANIES,
                payload: {
                    companies: res,
                }
            })
        }).catch(err => {
            dispatch({
                type: companies_constants.GET_COMPANIES_FAILURE,
                payload: null,
            })
        });
    }
}


//Action for getting a company by companyId
const getCompanyById = (id, todoOptions) => {
    return dispatch => {
        companies_services.getCompanyById(id, todoOptions).then(res => {
            let _todos = Utils.prioritizeTodos(res.todos);

            dispatch({
                type: companies_constants.GET_COMPANY_ID,
                payload: {
                    page_number: todoOptions.p_number,
                    company: {
                        ...res,
                        todos: _todos,
                        order: 'asc',
                    },
                }
            })
        })
    }
}

const uploadPDF = (file) => {
    return dispatch => {
        const fileReader = new FileReader();
        fileReader.onload = function (e) {
            companies_services.uploadPDF(file, fileReader)
                .then(res => {

                })
        }
        fileReader.readAsBinaryString(file);
    }
}

export default {
    addCompany,
    getCompanies,
    getCompanyById,
    uploadPDF,
}