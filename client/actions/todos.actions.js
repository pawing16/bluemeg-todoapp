import {todos_constants, companies_constants} from '../constants/';
import {todos_services} from '../services/';

import Utils from '../utils';

//Action for adding in a ToDo
function addTodo (payload) {
    return dispatch => {
        let todoId = Meteor.callPromise('todos.insert', {
            ...payload
          }).then( res => {
              dispatch({
                  type: companies_constants.ADD_TODO,
                  payload: {
                      _id: res,
                      ...payload,
                  }
              })

          }).catch( err => {
            dispatch({
                type: todos_constants.ADD_TODO_FAILURE,
                payload: null,
            })
          });
    }
}

//Action for getting all todos
function getTodos (){
    return dispatch => {
        let todos = todos_services.getTodos().then( res => {
            dispatch({
                type: todos_constants.GET_TODOS,
                payload: {
                    todos: res,
                }
            })
        }).catch( err => {
            dispatch({
                type: todos_constants.GET_TODOS_FAILURE,
                payload: null,
            })
        });
    }
}

//Action for toggling completion/done of a todo item
function toggleCompletion (id, isCompleted, companyId) {
    return (dispatch, getState) => {
        todos_services.toggleCompletion(id, !isCompleted)
        .then( res => {

            dispatch({
                type: companies_constants.UPDATE_TODO,
                payload: res,
            })
           
        })
    
    }
}

//Action for updating todo properties eg. Priorities, completion, description, etc.
function updateTodo(id, payload) {
    return (dispatch, getState) => {
        todos_services.updateTodo(id, payload)
        .then( res => {
            dispatch({
                type: companies_constants.UPDATE_TODO,
                payload: res,
            })
        })
    }
}


//Action for sorting todos by order and orderBy. note: sorting will be processed on FE
function sortTodos ( order, orderBy ) {
    return (dispatch, getState) => {

        const todosFromCompany = [...getState().companies.data.todos];
        if(orderBy === 'priority') {
            orderBy = 'priorityIndex';
        }
        let sorted = todosFromCompany.sort( Utils.sort(order, orderBy))

        dispatch({
            type: companies_constants.SORT_TODOS,
            payload: {
                todos: sorted,
                order,
            }
        })

    }
}

//Action for filtering todos. note: filtering will be processed on FE side
function filterTodos ( by, value ) {
    return (dispatch, getState) => {
        const todosFromCompany = [...getState().companies.data.todos];
        
        let filtered = todosFromCompany.filter(todo => {
            return todo[by] === value
        });
        
        dispatch({
            type: companies_constants.FILTER_TODOS,
            payload: {
                filtered
            }
        })
        
    }
}

export default {
    addTodo,
    updateTodo,
    getTodos,
    toggleCompletion,
    sortTodos,
    filterTodos,
}