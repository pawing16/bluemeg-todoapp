import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import reducer from './reducers';

//Main store of the App using the combined reducers and using thunkMiddleware to handle the async calls
const store = createStore( 
    reducer,
    applyMiddleware(
        thunkMiddleware
    )
)

export default store;