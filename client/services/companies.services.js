
import {companies_constants} from '../constants/';

import Utils from '../utils';
import { companies } from '../../shared/api/';
//Services for the company related infos and data, communicating to the Meteor defined methods using DDP instead of standard REST, but the flow is still the same using these services going to the reducers

function addCompany  (payload)  {
    let pdfReader = null;
    if( payload.pdfFile) {
        return new Promise( (resolve, reject) => {
            const pdfReader = new FileReader();
            pdfReader.onload = function(e){
                Meteor.callPromise('companies.insert', {
                    name: payload.name,
                    industry: payload.industry
                 }, {
                     file: payload.pdfFile.name,
                     result: pdfReader.result,
                 }).then( data => {
                     resolve(data);
                 }).catch( err => {
                     reject( err ); 
                 });
            };
            pdfReader.readAsBinaryString(payload.pdfFile);
        })

    } else {
        
        return Meteor.callPromise('companies.insert', {
            name: payload.name,
            industry: payload.industry
         });
    }
}

function getCompanies  () {
    return Meteor.callPromise('companies.fetch');
}


function getCompanyById  (id, {p_size, p_number}) {
    return Meteor.callPromise('companies.getById', {
        id,
        todoOptions:{
            p_size,
            p_number,
        }
    });
}

function uploadPDF(file, fileReader) {
    return Meteor.call('companies.uploadPDF', file, fileReader.result);
}

export default {
    addCompany,
    getCompanies,
    getCompanyById,
    uploadPDF,
}