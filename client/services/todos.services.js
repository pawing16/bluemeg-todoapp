
import {todos_constants} from '../constants/';

import { todos } from '../../shared/api/';

//Services for the todos related infos and data, communicating to the Meteor defined methods using DDP instead of standard REST, but the flow is still the same using these services going to the reducers


function addTodo  (payload)  {
    return Meteor.callPromise('todos.insert', {
        title: payload.title,
        description: payload.description,
        companyId: payload.companyId,
     });
}

function updateTodo(id, payload) {
    return Meteor.callPromise('todos.update', {
        id,
        ...payload,
    });
}

function removeTodo(id) {
    return Meteor.callPromise('todos.remove', {
        id,
    });
}

function getTodos  () {
    return Meteor.callPromise('todos.fetch');
}
function getTodosByCompanyId  (companyId, {p_size, p_number}) {
    return Meteor.callPromise('todos.getTodosByCompanyId', {
        companyId,
        p_size,
        p_number,
    });
}

function toggleCompletion(id, completed) {
    return Meteor.callPromise('todos.update', {
        id,
        completed,
    });
}

export default {
    addTodo,
    updateTodo,
    removeTodo,
    getTodos,
    toggleCompletion,
    getTodosByCompanyId,
}