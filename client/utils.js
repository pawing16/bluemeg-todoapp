//Mini utils function

const noop = () => {}

function sort(order, orderBy) {
    return order === 'desc'
      ? (a, b) => (b[orderBy] < a[orderBy] ? -1 : 1)
      : (a, b) => (a[orderBy] < b[orderBy] ? -1 : 1);
}


function prioritizeTodos(todos, sortIt = true, order = 'asc') {
    let _todos = todos.map( todo => {
        let priorityIndex = 1;
        if( todo.priority === 'medium') {
            priorityIndex = 2;
        } else if( todo.priority === 'low') {
            priorityIndex = 3;
        }
        return {
            ...todo,
            priorityIndex,
        }
    })
    return sortIt ? _todos.sort( sort( order, 'priorityIndex' )) : _todos;
}

function validateLength(value, option) {
    const { min, max } = option;
    return min <= value.length && value.length <= max;
};

function fileReader( file, cb = noop) {
    const fileReader = new FileReader();
    fileReader.onload = cb;
    fileReader.readAsBinaryString(file);

    return fileReader;
}

export default {
    noop,
    sort,
    prioritizeTodos,
    validateLength,
    fileReader,
}