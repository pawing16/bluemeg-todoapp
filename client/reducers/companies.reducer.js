import {companies_constants} from '../constants';

const initialState = {
    list: []
}

//These are the states for our company property

export default function( state = initialState , action) {

    switch(action.type) {
        case companies_constants.ADD_COMPANY : 
            return {
                ...state,
                list: [
                    ...state.list,
                    {
                        ...action.payload,
                    }
                ]
            }

        case companies_constants.ADD_TODO : 
            return {
                ...state,
                data: {
                    ...state.data,
                    todos: [
                        ...state.data.todos,
                        {
                            ...action.payload
                        }
                    ]
                }
            }

        case companies_constants.UPDATE_TODO : 

            const  updateTodo = todo => {
                if(todo._id === action.payload._id) {
                    todo = action.payload
                } 
                return todo;
            }

            let _todos = state.data.todos.map( updateTodo);

            let _filtered = state.data.filtered ? state.data.filtered.map(
                updateTodo
            ) : [];

            return {
                ...state,
                data: {
                    ...state.data,
                    todos: _todos,
                    filtered: _filtered
                }
            }

        case companies_constants.ADD_COMPANY_FAILURE : 
            return {
                ...state,
            }

        case companies_constants.GET_COMPANIES : 
            return {
                ...state,
                list: action.payload.companies
            }
        case companies_constants.GET_COMPANIES_FAILURE : 
            return {
                ...state,
            }

        case companies_constants.GET_COMPANY_ID : 
            let _todoList = [];
            let _page_loaded = false;
            if(action.payload.company.todos.length === 0) {
                if(action.payload.page_number > 1) {
                    _todoList = state.data.todos;
                } 
            } else {
                _todoList = action.payload.company.todos;
                _page_loaded = true;
            }
            return {
                ...state,
                data: {
                    ...action.payload.company,
                    todos: _todoList,
                    todoPageLoaded: _page_loaded
                }
            }

        case companies_constants.SORT_TODOS : 
            
            return {
                ...state,
                data: {
                    ...state.data,
                   todos: action.payload.todos,
                   order: action.payload.order,
                }
            }
        case companies_constants.FILTER_TODOS : 
            
            return {
                ...state,
                data: {
                    ...state.data,
                   filtered: action.payload.filtered,
                }
            }

        default:
            return {
                ...state
            }

    }
}