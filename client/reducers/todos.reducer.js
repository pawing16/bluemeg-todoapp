import {todos_constants} from '../constants';

const initialState = {
    list: []
}

//These are the states for our todo property

export default function( state = initialState , action) {

    switch(action.type) {
        case todos_constants.ADD_TODO : 
            return {
                ...state,
                list: [
                    ...state.list,
                    {
                        ...action.payload,
                    }
                ]
            }

        case todos_constants.ADD_TODO_FAILURE : 
            return {
                ...state,
            }

        case todos_constants.GET_TODOS : 
            return {
                ...state,
                list: action.payload.todos
            }
        case todos_constants.GET_TODOS_FAILURE : 
            return {
                ...state,
            }

        default:
            return {
                ...state
            }

    }
}