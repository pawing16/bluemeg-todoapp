import {combineReducers} from 'redux';

import todos from './todos.reducer';
import companies from './companies.reducer';

const reducer = combineReducers({
    companies,
    todos
})

export default reducer;