import React, {Fragment} from 'react';
import { render } from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Provider } from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';

import {CssBaseline} from '@material-ui/core';
 
import store from './store';

import App from './App.jsx';

//Using React, React router and redux for the UI, instantiate through Meteor startup method
Meteor.startup(() => {
  render(
    <Provider store={store}>
      <Router>
        <Fragment>
          <CssBaseline />
          <App />
        </Fragment>
      </Router>
    </Provider>
    , document.getElementById('root'));
});