import React, { Component, Fragment } from 'react';

import { Paper, Typography } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    node: {
        marginTop: '1em',
        marginBottom: '1em',
        position: 'relative',
    }
})

const ContentNode = props => {
    const { classes, label, children} = props;
    return (
        <div
            className={classes.node}
        >
            <Typography
                variant="caption"
            >
                {
                    label
                }
            </Typography>
            {
                typeof children === 'string' ? (
                    <Typography
                        variant="display1"
                    >
                        {children}
                    </Typography>
                ) : children
            } 
        </div>
    )

}

export default withStyles(styles)(ContentNode);