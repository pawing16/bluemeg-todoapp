import React, { Component, Fragment } from 'react';

import { withStyles } from '@material-ui/core/styles';
import { Table, TableBody, TableCell, TableHead, TableRow, Typography, Paper, Checkbox, TableSortLabel, Select, MenuItem, IconButton } from '@material-ui/core';
import {Tune, ArrowLeft, ArrowRight} from '@material-ui/icons/';

function getSorting(order, orderBy) {
    return order === 'desc'
      ? (a, b) => (b[orderBy] < a[orderBy] ? -1 : 1)
      : (a, b) => (a[orderBy] < b[orderBy] ? -1 : 1);
}

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        
    },
   
    tableWrapper: {
        overflowX: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
    },

    row: {
        cursor: 'pointer',
    },

    prioSelectRoot: {
        minWidth: 100,
    },
    prio_high: {
        color: '#ff1744',
        fontWeight: 'bold',
        fontSize: '0.8rem',
        textTransform: 'uppercase'
    },
    prio_medium: {
        color: '#ff9100',
        fontWeight: 'bold',
        fontSize: '0.8rem',
        textTransform: 'uppercase'
    },
    prio_low: {
        color: '#00a152',
        fontWeight: 'bold',
        fontSize: '0.8rem',
        textTransform: 'uppercase'
    },
});

const columnData = [
    { id: 'label', numeric: false, disablePadding: true, label: 'Name' },
    { id: 'description', numeric: false, disablePadding: true, label: 'Description' },
    { id: 'priority', numeric: false, disablePadding: true, label: 'Priority' },
];


class TodoHead extends Component {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const { order, orderBy } = this.props;
        return (
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">
                        Done
                    </TableCell>
                    {columnData.map(column => {
                        return (
                            <TableCell
                                key={column.id}
                                numeric={column.numeric}
                                sortDirection={orderBy === column.id ? order : false}
                            >

                                <TableSortLabel
                                    active={orderBy === column.id}
                                    direction={order}
                                    onClick={this.createSortHandler(column.id)}
                                >
                                    {column.label}
                                </TableSortLabel>

                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        )
    }
}

class TodoPriorityFilter extends Component {
    state = {
        filter: 'all'
    }

    handleChange = event => {
        this.setState( prevState => {
            return {
                filter: event.target.value
            }
        }, () => {
            this.props.filterHandler('priority', this.state.filter);
        })
    }
    render() {
        const {style} = this.props;
        return (
            <div 
                style={style}
            >
                   
                    <Tune 
                        style={{
                            display: 'inline-block',
                            verticalAlign: 'middle',
                            marginRight: '0.5em'
                        }}
                    />
                    
                    <div
                        style={{
                            display: 'inline-block',
                            verticalAlign: 'middle',
                        }}
                    >
                    <Typography
                        variant="caption"
                    >
                        Priority
                    </Typography>
                   
                        <Select
                            value={this.state.filter}
                            onChange={this.handleChange}
                            style={{
                                display: 'inline-block',
                                verticalAlign: 'middle',
                                minWidth: 100,
                            }}
                        >
                            <MenuItem value='all'>All</MenuItem>
                            <MenuItem value='high'>High</MenuItem>
                            <MenuItem value='medium'>Medium</MenuItem>
                            <MenuItem value='low'>Low</MenuItem>
                        </Select>
                    </div>
                    
                    
            </div>
        )
    }
}

class TodoPagination extends Component {

    static defaultProps = {
        nextDisabled: false,
        prevDisabled: false,
    }

    render() {
        return (
            <div style={
                {
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: '100%',
                    height: '45px',
                    display: 'flex',
                }
            }>
                <IconButton
                    disabled={this.props.prevDisabled}
                    onClick={()=>{
                        this.props.prevHandler()
                    }}
                >
                    <ArrowLeft />
                </IconButton>

                 <IconButton
                    disabled={this.props.nextDisabled}
                    onClick={()=>{
                        this.props.nextHandler()
                    }}
                 >
                    <ArrowRight />
                </IconButton>
            </div>
        )
    }
}

class TodoList extends Component {

    state = {
        order: 'asc',
        orderBy: 'label',
        filter: {
            by: 'priority',
            value: 'all'
        },
    }

    todoClickHandler = (event, id, isCompleted) => {
        this.props.todoClickHandler(id, isCompleted);
    }

    todoSortHandler = (event, _orderBy) => {
        const orderBy = _orderBy;
        let order = 'desc';

        if(this.state.orderBy === orderBy && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState( prevState => ({
            ...prevState,
            order,
            orderBy,
        }), () => {
            this.props.sortTodosHandler(order, _orderBy);
        })
    }

    todoFilterHandler = (by, value) => {
        this.setState( prevState => ({
            ...prevState,
            filter: {
                by,
                value
            }
        }))
        this.props.filterTodosHandler(by, value);
    }

    priorityChangeHandler = (event, id) => {
        this.props.todoUpdateHandler(id, {
            priority: event.target.value
        });
    }
    
    render() {
        const { classes, todos, page_loaded } = this.props;
        const { order, orderBy, filter} = this.state;
        
        let _doneTodos = (todos && todos.length > 0) ? todos.filter( todo => {
            return todo.completed
        }) : [];
        
        return (
            <Fragment>
                <Typography>
                        {
                            _doneTodos.length > 0 ? (
                                `${_doneTodos.length} out of ${todos.length} ${(filter.by === 'priority' &&  filter.value !== 'all') ? `${filter.value} priority ` : '' }todo/s are completed.`
                            ) : (
                                `You may want to start in doing your ${(filter.by === 'priority' &&  filter.value !== 'all') ? `${filter.value} priority ` : '' }todos.`
                            )
                        }
                </Typography>

               <TodoPriorityFilter 
                style={{
                    position: 'absolute',
                    top: 0,
                    right: 0,
                }}
                filterHandler={this.todoFilterHandler}
               />

                <Paper
                    className={classes.root}
                >
                    
                    <div
                        className={classes.tableWrapper}
                    >

                        {
                            (todos && todos.length > 0) ? (
                                <Fragment>
                                    <Table className={classes.table}>
                                        <TodoHead
                                            numSelected={2}
                                            order={order}
                                            orderBy={orderBy}
                                            onRequestSort={this.todoSortHandler}
                                            rowCount={5}
                                        />

                                        <TableBody>

                                            {
                                            
                                                    todos.map((item, itemIndex) => {
                                                            
                                                            
                                                            return (
                                                                <TableRow
                                                                    hover
                                                                    // onClick={event => this.todoClickHandler(event, item._id, item.completed)}
                                                                    role="checkbox"
                                                                    tabIndex={-1}
                                                                    key={`todo-item__${itemIndex}_${item.id}`}
                                                                    selected={item.completed}
                                                                    classes={
                                                                        {
                                                                            root: classes.row,
                                                                            selected: classes.selectedRow
                                                                        }
                                                                    }
                                                                >

                                                                    <TableCell padding="checkbox">
                                                                        <Checkbox 
                                                                            onChange={event => this.todoClickHandler(event, item._id, item.completed)} 
                                                                            checked={item.completed} 
                                                                        />
                                                                    </TableCell>

                                                                    <TableCell scope="row">
                                                                        {
                                                                            item.label && item.label !== '' ? item.label : '-'
                                                                        }
                                                                    </TableCell>

                                                                    <TableCell >
                                                                        {
                                                                            item.description && item.description !== '' ? item.description : '-'
                                                                        }
                                                                    </TableCell>

                                                                    <TableCell 
                                                                        className={classes[`prio_${item.priority}`]}
                                                                    >
                                                                        {
                                                                        
                                                                                
                                                                                <Select
                                                                                    value={item.priority}
                                                                                    onChange={event => this.priorityChangeHandler(event,item._id)}
                                                                                    classes={{
                                                                                        root: classes.prioSelectRoot,
                                                                                        selectMenu: classes[`prio_${item.priority}`]
                                                                                    }}
                                                                                    
                                                                                >
                                                                                    <MenuItem value='high'>High</MenuItem>
                                                                                    <MenuItem value='medium'>Medium</MenuItem>
                                                                                    <MenuItem value='low'>Low</MenuItem>
                                                                                </Select>
                                                                            
                                                                        }

                                                                    </TableCell>

                                                                </TableRow>
                                                            )
                                                        })

                                                
                                            }

                                        </TableBody>
                                    </Table>

                                    <TodoPagination 
                                        nextHandler={()=>{
                                            this.props.loadNextHandler()
                                        }}
                                        prevHandler={()=>{
                                            this.props.loadPrevHandler()
                                        }}

                                        nextDisabled={!page_loaded}
                                        prevDisabled={false}
                                    />
                                </Fragment>
                            ) : (
                            
                                                
                                                    <Typography
                                                        variant="caption"
                                                        style={{
                                                            padding: '2em'
                                                        }}
                                                    >

                                                        Todo's are empty, you may want to add your todos for this company. Please fill up the todo form. :)    
                                                    
                                                    </Typography>
                                                
                                        
                            )
                        }

                        

                    </div>
                </Paper>
            </Fragment>
        )
    }
}

export default withStyles(styles)(TodoList);