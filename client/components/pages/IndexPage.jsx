import React, { Component, Fragment } from 'react';

import {connect} from 'react-redux';

import { Paper } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles';

import {todos_actions, companies_actions} from '../../actions';

import CompanyForm from '../CompanyForm.jsx';

const styles = theme => ({
    page: {
        padding: '2em',
    }
})

class IndexPage extends Component {
    companyFormSubmitHandler = (payload) => {
        this.props.dispatch( companies_actions.addCompany(payload) );
    }

    uploadPdfHandler = (file) => {
        this.props.dispatch( companies_actions.uploadPDF(file) );
    }
    render() {
        //IndexPage, containing the form for adding up a company, and populating the lists of companies
        const {classes} = this.props;
        return (
            
                <Paper className={classes.page}>

                    <CompanyForm 
                        uploadPdfHandler={this.uploadPdfHandler}
                        onSubmit={this.companyFormSubmitHandler}
                    />

                </Paper>
           
        )
    }
}

const mapStateToProps = state => ({
    todos: state.todos,
    companies: state.companies,
})

export default withStyles(styles)(connect(mapStateToProps)(IndexPage));