import React, { Component, Fragment } from 'react';

import {connect} from 'react-redux';

import { Paper, Typography, Divider } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles';

import {todos_actions, companies_actions} from '../../actions';

import TodoForm from '../TodoForm.jsx';
import TodoList from '../TodoList.jsx';
import ContentNode from '../ContentNode.jsx';

const styles = theme => ({
    page: {
        padding: '2em',
        backgroundColor: '#f3f3f3'
    },
    column: {
        width: '50%',
        flexBasis: '50%',
        flexGrow: 1,
        height: '100%',
        padding: '1em',
    },
    companyColumn: {
        paddingRight: 'calc(360px + 1em)',
        backgroundColor: '#f3f3f3'
    },
    todoForm: {
        width: '360px',
        flexBasis: '360px',
        backgroundColor: '#fdfbe4',
        position: 'fixed',
        top: 0,
        bottom: 0,
        right: 0,
    },
    companyDetails: {
        paddingBottom: '2em',
        flexBasis: '50%',
    },
    companyDetailsWrap: {
        display: 'flex',
        flexDirection: 'row',
    }
})

class CompanyPage extends Component {

    state = {
        companyId: this.props.match.params.companyId,
        todoOptions: {
            p_size: 7,
            p_number: 1,
        }
    }

    todoSubmitHandler = (payload) => {
        this.props.dispatch( todos_actions.addTodo(payload) );
    }

    todoClickHandler = (id, isCompleted) => {
        this.props.dispatch( todos_actions.toggleCompletion(id, isCompleted, this.state.companyId ) );
    }

    todoUpdateHandler = (id, payload) => {
        this.props.dispatch( todos_actions.updateTodo(id, payload ) );
    }

    todosSort = ( order = 'asc', orderBy = 'label' ) => {
        this.props.dispatch( todos_actions.sortTodos(order, orderBy) );
    }

    todosFilter = (by, value) => {
        this.props.dispatch( todos_actions.filterTodos(by, value) );
    }

    todoLoadNext = () => {
        this.setState( prevState => ({
            ...prevState,
            todoOptions: {
                ...prevState.todoOptions,
                p_size: prevState.todoOptions.p_size,
                p_number: prevState.todoOptions.p_number + 1,
            }
        }), () => {
            this._loadCompany(this.props)
        })
    }

    todoLoadPrev = () => {
        this.setState( prevState => ({
            ...prevState,
            todoOptions: {
                ...prevState.todoOptions,
                p_size: prevState.todoOptions.p_size,
                p_number: prevState.todoOptions.p_number <= 1 ? 1 : prevState.todoOptions.p_number - 1, 
            }
        }), () => {
            this._loadCompany(this.props)
        })
    }

    _loadCompany(props) {
        const {params}  = props.match;
        const {companyId} = params;

        const {todoOptions} = this.state;

        if(companyId) {
            props.dispatch(companies_actions.getCompanyById(companyId, {
                p_size: todoOptions.p_size,
                p_number: todoOptions.p_number,
            }));
        }

        this.setState( prevState => ({
            ...prevState,
            loaded: true
        }))
    }

    _getTodos(companyData) {
        if(!companyData || (companyData && companyData.todos && companyData.todos.length === 0)) {
            return [];
        }
        
        return (companyData.filtered && companyData.filtered.length > 0) ? companyData.filtered : companyData.todos
    }

    shouldComponentUpdate(nextProps, nextState) {
       
        if(nextProps.companies === this.props.companies) {
           return false;
        }
        return true;
        
    }

    componentWillReceiveProps(nextProps) {
       
        if(nextProps.location.pathname !== this.props.location.pathname) {
            this.setState( prevState => ({
                ...prevState,
                companyId: nextProps.match.params.companyId,
                todoOptions: {
                    p_size: 7,
                    p_number: 1,
                }
            }), () => {
                this._loadCompany(nextProps);
            })
        }
    }

    componentDidMount() {
        this._loadCompany(this.props);
    }
   
    render() {
        const {classes, companies} = this.props;
        return (
            <Fragment>
                {
                    companies && companies.data && (
                        <section 
                            className={`${classes.column} ${classes.companyColumn}`}
                        >
                            <Typography
                                variant="subheading"
                            >
                                Company
                            </Typography>

                            <section
                                className={classes.companyDetailsWrap}
                            >
                                <div className={classes.companyDetails}>
                                    <ContentNode
                                        label='Name'
                                    >
                                       <Typography variant="headline" > {companies.data.name} </Typography>
                                    </ContentNode>

                                    <ContentNode
                                        label='Industry'
                                    >
                                        <Typography variant="headline" >{companies.data.industry}</Typography>
                                    </ContentNode>
                                </div>

                                <div className={classes.companyDetails}>

                                    <Typography>
                                        Authorized Representatives
                                    </Typography>
                                    {
                                        companies.data.representatives && companies.data.representatives.length > 0 ? companies.data.representatives.map( (delegate, repInd) => {
                                            if(delegate.id && delegate.name && delegate.position) {
                                            return (
                                                <ContentNode
                                                    key={`rep__${repInd}`}
                                                    label={delegate.id}
                                                >
                                                    <Typography>
                                                        {`${decodeURIComponent(delegate.name)} - ${decodeURIComponent(delegate.position)}`}
                                                    </Typography>
                                                </ContentNode>
                                            )
                                            }
                                            return null;
                                        }) : null
                                    }
                                </div>

                            </section>


                            <Divider light />

                            <ContentNode
                                label='Todos'
                            >
                               <TodoList 
                                    page_loaded = {companies.data.todoPageLoaded}
                                    todos={this._getTodos(companies.data)} 
                                    todoClickHandler={this.todoClickHandler}
                                    todoUpdateHandler={this.todoUpdateHandler}
                                    sortTodosHandler={this.todosSort}
                                    filterTodosHandler={this.todosFilter}

                                    loadNextHandler={this.todoLoadNext}
                                    loadPrevHandler={this.todoLoadPrev}
                               />
                            </ContentNode>
                        </section>
                    )
                }
                <Paper 
                    className={`${classes.column} ${classes.todoForm}`}
                >
                    
                    <TodoForm 
                        onSubmit={this.todoSubmitHandler}
                        companies = { (companies && companies.list && companies.list.length > 0) ? companies.list.map( company => ({
                            _id: company._id,
                            name: company.name,
                        })) : []}
                        companyId = {
                            companies.data ? companies.data._id : ''
                        }
                        selectCompany = {false}
                        
                    />
                </Paper>
            </Fragment>
           
        )
    }
}

const mapStateToProps = state => ({
    todos: state.todos,
    companies: state.companies,
})

export default withStyles(styles)(connect(mapStateToProps)(CompanyPage));