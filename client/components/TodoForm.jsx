import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import { Button, Typography, Paper, TextField, Select, FormControl, FormControlLabel, FormLabel, InputLabel, MenuItem, RadioGroup, Radio } from '@material-ui/core';

import Utils from '../utils';

const styles = theme => ({
    form: {
        ...theme.mixins.gutters(),
        paddingBottom: theme.spacing.unit * 2,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 300,
    },
    select: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginTop: theme.spacing.unit,
        width: 300,
    },

    radios: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginTop: '2em',
        width: 300,
    },

    group: {
        flexDirection: 'row',
    },
    button: {
        margin: theme.spacing.unit,
        marginTop: '1.5em',
    },
})

class TodoForm extends Component {
    state = {
        data: {
            label: '',
            description: '',
            companyId: this.props.companyId || '',
            priority: 'low',
            completed: false,
        },
        validate: {
            label: {
                length: {
                    min: 5,
                    max: 300
                },
                isValid: false,
            },
            description: {
                length: {
                    min: 5,
                    max: 500
                },
                isValid: false,
            },
        },
        allAreValid: false,
    }

    _resetForm() {
        this.setState( prevState => ({
            ...prevState,
            data: {
                label: '',
                description: '',
                companyId: this.props.companyId || '',
                priority: 'low',
                completed: false,
            }
        }))
    }

    _validateLength(name, value) {
        //Validate the length of the field input value
        let {validate} = this.state;
        let _validate = validate[name];
        if(_validate) {
            return Utils.validateLength(value, {
                min: _validate.length.min,
                max: _validate.length.max,
            })
        }
        return true;
    }

    _validationsAreValid = (name, isRequired) => {
        const {validate, data} = this.state;
        let allAreValid = true;
    
        Object.keys(validate).forEach( item => {
            if(!validate[item].isValid) {
                allAreValid = false;
                if(item === name && !isRequired ) {
                    if(data[item] === '' || !data[item]) {
                        allAreValid = true;
                    }
                }
            }
        })

        return allAreValid;
    }

    handleChange = (name, isRequired = false) => event => {
    
        let value = event.target.value;
        this.setState(prevState => {
            let validate;
            if(prevState.validate[name]) {
                validate = {
                    [name]: {
                        ...prevState.validate[name],
                        isValid: this._validateLength(name, value)
                    }
                }
            }
            return {
                ...prevState,
                data: {
                    ...prevState.data,
                    [name]: value,
                },
                validate:{
                    ...prevState.validate,
                    ...validate,
                }
            }
        }, () => {

            this.setState( prevState => ({
                ...prevState,
                allAreValid: this._validationsAreValid(name, isRequired)
            }))

        })
        
    };

    handleSubmit = event => {
        event.preventDefault();
        let data = this.state.data; 
        
            this.props.onSubmit && (
                this.props.onSubmit(data)
            )
            this._resetForm();
        
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.companyId !== this.props.companyId) {
            this.setState( prevState => ({
                ...prevState,
                data: {
                    ...prevState.data,
                    companyId: nextProps.companyId,
                }
            }))
        }
    }

    render() {
        const { classes, companies, selectCompany } = this.props;
        const {validate, allAreValid} = this.state;
        const { label, description, companyId, priority } = this.state.data;

        // if(!priority || priority === '') && (priority = 'low')

        return (
            <form
                className={classes.form}
                noValidate={false}
                autoComplete="off"
                onSubmit={this.handleSubmit}
            >
                 <FormControl>
                    <TextField
                        required
                        error={ label !== '' && !validate.label.isValid}
                        id="label"
                        label="Todo Label"
                        value={label}
                        className={classes.textField}
                        margin="normal"
                        onChange={this.handleChange('label', true)}
                        helperText={!validate.label.isValid ? `Todo label must be at least ${validate.label.length.min} characters` : null}
                    />
                 </FormControl>
                <FormControl>
                    <TextField
                        error={ description !== '' && !validate.description.isValid}
                        id="description"
                        label="Todo Description"
                        value={description}
                        className={classes.textField}
                        margin="normal"
                        onChange={this.handleChange('description')}
                        helperText={ !validate.description.isValid ? `Todo description must be at least ${validate.description.length.min} characters` : null}
                    />
                </FormControl>
                {
                    selectCompany && (
                        <FormControl className={classes.select}>
                            <InputLabel htmlFor="company">Company</InputLabel>
                            <Select
                                required
                                value={companyId}
                                onChange={this.handleChange('companyId', true)}
                                inputProps={{
                                name: 'company',
                                id: 'company',
                                }}
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                {
                                    companies.length > 0 && (
                                        companies.map( company => (
                                            <MenuItem 
                                                key={`todo_company_item__${company._id}`}
                                                value={company._id}
                                            >
                                                {company.name}
                                            </MenuItem>
                                        ))
                                    )
                                }
                            </Select>
                        </FormControl>
                    )
                }
                

                <FormControl className={classes.radios} required >
                    <FormLabel component="legend">Priority</FormLabel>
                    <RadioGroup
                        name="priority"
                        className={classes.group}
                        value={priority || 'low'}
                        onChange={this.handleChange('priority')}
                    >
                        <FormControlLabel checked={priority === 'high'} value="high" control={<Radio />} label="High" />
                        <FormControlLabel checked={priority === 'medium'} value="medium" control={<Radio />} label="Medium" />
                        <FormControlLabel checked={priority === 'low'} value="low" control={<Radio />} label="Low" />
                        
                    </RadioGroup>
                </FormControl>

                <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    type="submit"
                    disabled={!allAreValid}
                >
                    Add Todo
                </Button>

            </form>
        )
    }
}

export default withStyles(styles)(TodoForm)