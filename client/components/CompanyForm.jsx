import React, { Component } from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import { Button, Typography, Paper, TextField, Input } from '@material-ui/core';


const styles = theme => ({
    form: {
        ...theme.mixins.gutters(),
        paddingBottom: theme.spacing.unit * 2,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 250,
    },
    button: {
        margin: theme.spacing.unit,
        marginTop: '1.5em',
    },
})

class CompanyForm extends Component {
    state = {
        data: {
            name: '',
            industry: '',
            pdfFile: '',
        }
    }

    _resetForm() {
        this.setState( prevState => ({
            ...prevState,
            data: {
                name: '',
                industry: '',
                pdfFile: '',
            }
        }))
    }

    handlePdfUpload = event => {
        let pdfFile = event.target.files[0];
        this.setState( prevState => ({
            ...prevState,
            data: {
                ...prevState.data,
                pdfFile,
            }
        }))

    //    this.props.uploadPdfHandler( pdfFile )
        
    }

    handleChange = name => event => {
        let value = event.target.value;
        this.setState(prevState => {
            return {
                ...prevState,
                data: {
                    ...prevState.data,
                    [name]: value,
                }
            }
        })
    };

    handleSubmit = event => {
        event.preventDefault();
        // console.log('# signup submit', this.state.data);
        
        this.props.onSubmit && (
            this.props.onSubmit(this.state.data)
            
        )

        this._resetForm();
    }

    render() {
        const { classes } = this.props;
        const { name, industry } = this.state.data;
        return (
            <form
                className={classes.form}
                noValidate
                autoComplete="off"
                onSubmit={this.handleSubmit}
            >

                <Input
                    type='file'
                    onChange={this.handlePdfUpload}
                />

                <TextField
                    required
                    id="name"
                    label="Company Name"
                    value={name}
                    className={classes.textField}
                    margin="normal"
                    onChange={this.handleChange('name')}
                />

                <TextField
                    required
                    id="industry"
                    label="Company Industry"
                    value={industry}
                    className={classes.textField}
                    margin="normal"
                    onChange={this.handleChange('industry')}
                />

                <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    type="submit"
                >
                    Register Company
                </Button>

            </form>
        )
    }
}

export default withStyles(styles)(CompanyForm)